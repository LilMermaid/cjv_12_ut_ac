# Projet Session Level Design - Cohorte 12

## Setup du git


```bash
https://lecegeplimoilou-my.sharepoint.com/:v:/g/personal/pierre-a_leclerc_cegeplimoilou_ca/EStu6yuDIwNFoLITJTyiaWsBkdWpxmiPA07yLMSn2ASn-g?e=cvw6uH
```

## Ordre du push

```git
git pull
git add .
git commit -m "Comment"
git push
```

## Contribution:

Level_01_VortexRiker_BrunoHenriTremblay\
Level_02_MiningResearchFacility_AnnabelleLordPare\
Level_03_Farm_KevenBedard\
Level_04_BoreholeRobot_PDusablon\
Level_05_AngelArturoOrtizMartinez\
Level_06_OvergrownTower_SRoberts\
Level_07_Eros_SebastienGouletPoulin\
Level_08_ProjectT-A1_KelleeHill\
Level_09_BeautifulDisaster_RashelDegarie\
Level10_Floodgates_GabrielTremblay\
Level_11_IrikokForest_AlexandreBergeron\
Level_12_Hivemind_SimonBanville\
Level_13_Biodôme_VincentBoucher\
Level_14_LaunchSite_GabrielChabot\
Level_15_Casino_ArielCarignan\
Level_16_OilPlatform_KevinBachelier\
Level_17_Shotengai_MarcAntoineGermain\
Level_18_AlexisDumas\
Level_19_KingsHill_AlexisBourboin\
Level_20_Seabase_AnnJulieLanglois\
Level_21_AxonFactory_TommyBouchard\
Level_22_Door_to_Hell_AlexDufour\
Level_23_BlissNightclub_LeandreDuchesneau\
Level_24_Azteca_JanickMurrayHall\
Level_25_Cathedrale_JeanFrancoisLauzon\
Level_26_ Highrise_GuillaumeLehoux\
Level_27_Mayhem_DaveBernier

## License
Epic Tournament 